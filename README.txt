%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Documentation
%   Michelangelo Bin & Thomas Parisini
%   24 November 2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

1. Organisation
The root folder contains the scripts:
 - Agent.m: a class file containing the basic methods and properties of each individual of the agent-based simulaton model
 - arrayContains.m: an auxiliary function checking whether an array contains a given value
 - Distribution1D.m: a class managing sampling from 1D distributions
 - imageCreation.m: a script generating graphic objects from simulation data (see below)
 - main*.m: a set of scripts launching repeated simulations with different strategies (see below)
 - paperImageCreator.m: a variation of imageCreation.m used to generate the actual images of the paper (see below)
 - SamplingManager.m: a class containings methods and properties used for testing and measurements
 - simulator.m: the script performing the actual simulations (see below)
 - SystemObserver.m: a class containing methods for statistics gathering during simulation

The "data" folder contains the output of the main*.m files, i.e. the actual simulation output. It contains files simout_*.mat, which are the data used in the paper. They will be overwritten by a new execution of the main*.m files.
The "data/temp" folder contains temporary auxiliary .mat files needed by the main*.m files during execution. Again, they will be overwritten by a new executon.
The "data/backup_paper_data" contains a backup of the output data used in the paper. These are copies of the original data/simout_*.mat files and are not overwritten by a new execution.

The "figures" folder contains the output of paperImageCreator.m, in form of tikz .tex files.
The files "figures/boxplot.tex" and "figures/timeseries.tex" are the files used in the paper creation.
The files "figures/boxplot_standalone.tex" and "figures/timeseries_standalone.tex" are identical copies wrapped into standalone tex code. Namely, they can be directly compiled.


2. About simulator.m
This is a file performing the simulation of the outbreak. It can be executed as standalone, in which case it uses the default setting written in the same file. The file is commented.


3. About main*.m
The files main*.m, * in {'',2,3,4,5,6}, are mutually independent files in which different repetitions of simulations using the same testing/vaccination strategy are launched, and the corresponding output saved to the "data" folder. These contain repeated calls to the simulator.m file with settings different from defalut ones. The files are commented.

Each file can run different strategies sequentially. The scripts are independent and can be executed in parallel. Indeed, the reason of using different copies is to allow running them in parallel on different matlab processes. This allows a "naive" parallelisation managed by the OS, without adapting the code and suffering from communication overhead. 

Each file runs the simulations with basic settings contained in the array EXT_simulation_data. For instance,
EXT_simulation_data = { 
  [SamplingManager.PAGERANK SamplingManager.MOSTPROBABLE 50/70 100]
}
stands for 100 repetitions by using 
 - for Testing Phase 1, the Pagerank algorithm for 50 daily tests (over 70 total).
 - for Testing Phase 2, the current-state "Most Probable" method for the remaining 20 daily tests.


4. About imageCreation.m
This file creates images from the simulation output data contained in the folder "data". The file is commented.


4. About paperImageCreator.m
This file is a variation of imageCreation.m used to generate the images embedded in the paper.
It employs "matalab2tikz" to convert the matlab .fig output into .tex tikz file. This plugin can be accessed at
https://github.com/matlab2tikz/matlab2tikz
The file is commented.
