classdef SamplingManager < handle
 
    properties  (Constant)
        %modes
        NOTESTING = 0;
        % test-to-isolate
        RANDOM=1;
        MOSTPROBABLE=2;
        MIN_PROJENTROPY=3;
        MAX_PROJPROB = 4;
        MAX_EIGENVALUE=5;
        
        %graph based
        CONNECTED=6;
        CONNECTED_WEIGHTED=7;
        PAGERANK = 8;
        CLOSENESS=9;
        BETWEENESS=10;
        KEMENY = 11;
        PAGERANK_PROB = 12;
        WEIGHTED_PROB = 13;
        
       
         
        % outcomes
        OUTCOME_NEGATIVE=0;
        OUTCOME_POSITIVE=1;
        
        
          
         
    end
    
    properties  
        
        % Testing
         sampling_mode; 
         gamma = @(prob) sum(abs(prob(:)));

        % Phase estimation
         Phases;   % phase estimates
         cpf;      % conditional probability estimation function
         
        % String representation id+1
        TEST_MODE_STRINGS = ["No Testing";"Random";"Most Probable";"Min Proj Entropy";"Max Proj Probability";...
              "Max Eigenvalue";"Node Degree";"Weighted Node Degree";"Pagerank";...
              "Closeness";"Betweeness";"Kemeny";"Prob-Pagerank";"Weighted-Prob"];
         
        TEST_MODE_ABBREV = ["NT";"R";"MP";"MPE";"MPP";"ME";"ND";"WND";"PR";"CL";"BT";"K";"pP";"W"];

          
    end
    
    
    methods (Static)
          
         
        %% entropy
        
        function ie = entropy(p) 
        	ie = -p.*log(p)-(1-p).*log(1-p);
            ie(isnan(ie))=0;
        end 
        
        function E = totalIndividualEntropy(P)
            E = sum(entropy(P));
        end
          
        %% Testing and Quaratining
 
        function e = agentEligibleForTesting(agent,t)
            global TESTING_REFRACTORY_PERIOD 
            e = ~(agent.isVaccinated() || ... 
                  agent.isQuarantined() || ...      
                  (agent.wasQuarantined() && agent.quarantined_because_infected) || ...
                  (agent.wasTested() && t<=agent.last_test_time+TESTING_REFRACTORY_PERIOD) ...
                 );
        end
  
        function quarantined = quarantineAgents(IDs,Agents,time) 
            if isempty(IDs)
                quarantined=[];
                return;
            end
            
            quarantined = NaN(size(IDs));
            m=1;
            for id=IDs(:)'
                if Agents{id}.testsPositive() && ~Agents{id}.isQuarantined()
                    Agents{id}.activateQuarantine(time,true);
                    quarantined(m)=id;
                    m=m+1; 
                end
            end 
            
            quarantined = rmmissing(quarantined);
            
        end
        
        function [V,Q] = vaccinateOrQuarantineAgents(IDs,Agents,time,max_v) 
            if isempty(IDs)
                V=[];
                Q=[];
                return;
            end
            
            Q = NaN(size(IDs));
            V = Q;
            
            mQ=1;
            mV=1;
            for id=IDs(:)'
                if Agents{id}.testsPositive() && ~Agents{id}.isQuarantined()
                    Agents{id}.activateQuarantine(time,true);
                    Q(mQ)=id;
                    mQ=mQ+1; 
                elseif Agents{id}.eligibleForVaccine() && mV<=max_v
                    Agents{id}.setVaccined(time);
                    V(mV)=id;
                    mV=mV+1;
                end
            end 
            
            Q=rmmissing(Q);
            V=rmmissing(V);
            
        end
         

 
        
    end
    
%% constructors   
    
    methods
        
        function obj = SamplingManager(P0,cpf0)  
            obj.Phases = P0;
            obj.cpf=cpf0;
        end
        
        function setSamplingMode(obj,mode)
            obj.sampling_mode = mode;
        end
        
        function P = getProbabilities(obj)
            P = obj.Phases;
        end
        
        
%% Testing   

        function IDs = randomTesting(obj,Agents,M,excluded,t)
           global N
           
           IDs = NaN(M,1); 
           feasible = setdiff((1:N)',excluded); 
           m=1; 
            
           while m<=M && numel(feasible)>0
               i = 1+floor(rand()*numel(feasible));
               if  SamplingManager.agentEligibleForTesting(Agents{feasible(i)},t)
                    IDs(m)=feasible(i);
                    m = m+1;
               end
               feasible(i)=[];
           end
           
           IDs = rmmissing(IDs);
            
        end
          
        function IDs = ConnectednessTesting(obj,Agents,M,excluded,t)
           global N
           feasible = setdiff((1:N)',excluded);
           Nf = numel(feasible);
           A=NaN(Nf,1);
           V=A;
           n=1;
           
           for id=feasible(:)'
               if SamplingManager.agentEligibleForTesting(Agents{id},t)
                    A(n)= id;
                    V(n)= Agents{id}.getNumberOfContacts();
                    n=n+1;
               end     
           end
           
           [A,I]=rmmissing(A);
           V(I)=[];
           [V,I]=rmmissing(V);
           A(I)=[];
           
           [~,idx] = sort(V,'descend');
           A = A(idx); 
           IDs=A(1:min(M,end),1); 
            
        end
        
        function IDs = WeightedConnectednessTesting(obj,Agents,M,excluded,t)
           global N
           feasible = setdiff((1:N)',excluded);
           Nf = numel(feasible); 
           A=NaN(Nf,1);
           V=A;
           n=1;
           
           for id=feasible(:)'
               if SamplingManager.agentEligibleForTesting(Agents{id},t)
                   A(n)= id;
                   V(n)= sum(Agents{id}.contacts_values);
                   n=n+1;
               end
           end 
           
           [A,I]=rmmissing(A);
           V(I)=[];
           [V,I]=rmmissing(V);
           A(I)=[];
           
           [~,idx] = sort(V,'descend'); 
           A = A(idx); 
           IDs=A(1:min(M,end),1); 
            
        end
        
        function IDs = MostProbableTesting(obj,Agents,M,excluded,t)
            global N
              
            [~,sIDs]=sort(obj.Phases,'descend');
            
            IDs = nan(M,1);
            m=1;
            i=1;
            while m<=M && i<=N 
                if ~arrayContains(excluded,sIDs(i)) && SamplingManager.agentEligibleForTesting(Agents{sIDs(i)},t)
                    IDs(m)=sIDs(i);
                    m=m+1;
                end
                i=i+1;
            end
            
            IDs = rmmissing(IDs);
            
        end
        
        function IDs = minimumPostEntropyTesting(obj,Agents,M,excluded,t)  
            global N
            feasible = setdiff((1:N)',excluded);
            Nf = numel(feasible);
            A = NaN(Nf,1);
            V=A;
             
            n=1;
            for fid=feasible(:)'
                if SamplingManager.agentEligibleForTesting(Agents{fid},t)
                    A(n) = fid;
                    Pe_1 = obj.projectPhases(Agents,fid,SamplingManager.OUTCOME_POSITIVE);
                    Pe_0 = obj.projectPhases(Agents,fid,SamplingManager.OUTCOME_NEGATIVE);
                    
                    V(n) = SamplingManager.totalIndividualEntropy(Pe_1)*obj.Phases(fid) ...
                                + SamplingManager.totalIndividualEntropy(Pe_0)*(1-obj.Phases(fid));
                    n = n+1;
                end
            end
             
            
            [A,I]=rmmissing(A);
            V(I)=[];
            [V,I]=rmmissing(V);
            A(I)=[];
           
            fprintf('\n\t min ent: mean %2.3f, std %2.3f, min %2.3f, max %2.3f',mean(V),std(V),min(V),max(V));
            
            [~,idx] = sort(V,'ascend');
            A = A(idx); 
            IDs=A(1:min(M,end),1);   
    
        end
           
        function IDs = maxProjectedProbabilityTesting(obj,Agents,M,excluded,t)
            
            global N
            feasible = setdiff((1:N)',excluded);
            Nf = numel(feasible);
            A = NaN(Nf,1);
            V=A;
             
            n=1;
            for fid=feasible(:)'
                if SamplingManager.agentEligibleForTesting(Agents{fid},t)
                    A(n) = fid;
                    Pe_1 = obj.projectPhases(Agents,fid,SamplingManager.OUTCOME_POSITIVE);  
                    V(n) = (obj.gamma(Pe_1)-obj.gamma(obj.Phases))*obj.Phases(fid); 
                    n = n+1;
                end
            end
             
            
            [A,I]=rmmissing(A);
            V(I)=[];
            [V,I]=rmmissing(V);
            A(I)=[];
           
            [~,idx] = sort(V,'descend');
            A = A(idx); 
            IDs=A(1:min(M,end),1);   
            
        end
        
        function G = constructCorrelationGraph(obj,Agents)
            global N 
            A = zeros(N,N); 
            for i=1:N
                 
                for j=1:N
                    
                    if j==i; continue; end
                    
                    pi = obj.Phases(i);
                    pj = obj.Phases(j);
                    w_ij = (obj.conditionalProbability(Agents,i,j)+obj.conditionalProbability(Agents,j,i))/2;
                     
                    if w_ij>(pi+pj)/2
                        A(i,j) = w_ij;
                    end
                     
                end 
                
            end
            
            G = graph(A,string((1:N)'));
        
        end
         
        function IDs = centralityTesting(obj,Agents,M,excluded,mode,weights,t,prob)
            
            G = obj.constructCorrelationGraph(Agents); 
            if weights
                G.Nodes.PageRank = centrality(G,mode,'Cost',G.Edges.Weight);
            else
                G.Nodes.PageRank = centrality(G,mode,'Importance',G.Edges.Weight);
            end
            if prob
                [~,idx] = sort(G.Nodes.PageRank.*obj.Phases,'descend');
            else 
                [~,idx] = sort(G.Nodes.PageRank,'descend'); 
            end
            Nms = G.Nodes.Name(idx);
            
            IDs=NaN(M,1);
            m=1;
            i=1;
            while m<=M && i<=numel(Nms)
                id = round(str2double(Nms{i}));
                if ~arrayContains(excluded,id) && SamplingManager.agentEligibleForTesting(Agents{id},t)
                    IDs(m)=id;
                    m=m+1;
                end
                i=i+1;
            end
            
            V =  G.Nodes.PageRank;
            fprintf('\n\t rank: mean %2.3f, std %2.3f, min %2.3f, max %2.3f',mean(V),std(V),min(V),max(V));
           
            IDs = rmmissing(IDs);
            
        end
         
        function IDs = kemenyTesting(obj,Agents,M,excluded,t)
            
            G = obj.constructCorrelationGraph(Agents);  
            Nf = size(G.Nodes,1); 
              
            Candidates=NaN(Nf,1);
            V=Candidates;
            m=1;
            for i=1:Nf
                id = round(str2double(G.Nodes.Name{i}));
                if ~arrayContains(excluded,id) && SamplingManager.agentEligibleForTesting(Agents{id},t)
                    
                    Candidates(m)=id;
                    
                    rG = G.rmnode(G.Nodes.Name{i});
                    
                    rA = rG.adjacency('weighted');
                    for j=1:Nf-1
                        s = sum(rA(j,:));
                        if s>0
                            rA(j,:) = rA(j,:)/s;
                        end
                    end
                    
                    eig = eigs(rA);
                    eig = sort(rmmissing(eig),'descend');
                    eig = eig(2:end);
                    if isempty(eig)
                        K=0;
                    else
                        K=sum(1./(1-eig));
                    end
                    
                    V(m) = K;
                    m=m+1;
                end
            end
            
            [Candidates,I]=rmmissing(Candidates);
            V(I)=[];
            [V,I]=rmmissing(V);
            Candidates(I)=[];
            
            [~,idx] = sort(V,'descend');
            Candidates = Candidates(idx);
            
            IDs=Candidates(1:min(M,end),1);
             
            
        end
  
        function IDs = weightedProbTesting(obj,Agents,M,excluded,t) 
           global N
           feasible = setdiff((1:N)',excluded);
           Nf = numel(feasible); 
           A=NaN(Nf,1);
           V=A;
           n=1;
           
           for id=feasible(:)'
               if SamplingManager.agentEligibleForTesting(Agents{id},t)
                   A(n)= id;
                   V(n)=0;
                   if Agents{id}.hasContacts()
                       for j=Agents{id}.contacts_agents(:)'
                           V(n) = V(n) + obj.conditionalProbability(Agents,id,j)*obj.Phases(j);
                       end
                   end
                   n=n+1;
               end
           end 
           
           [A,I]=rmmissing(A);
           V(I)=[];
           [V,I]=rmmissing(V);
           A(I)=[];
           
           [~,idx] = sort(V,'descend'); 
           A = A(idx); 
           IDs=A(1:min(M,end),1); 
        end
        
        function IDs = getTestIDs(obj,Agents,M,excluded,t) 
            
            if obj.sampling_mode == SamplingManager.RANDOM
                IDs = obj.randomTesting(Agents,M,excluded,t); 
            elseif obj.sampling_mode == SamplingManager.CONNECTED
                IDs = obj.ConnectednessTesting(Agents,M,excluded,t);
            elseif obj.sampling_mode == SamplingManager.CONNECTED_WEIGHTED
                IDs = obj.WeightedConnectednessTesting(Agents,M,excluded,t);
            elseif obj.sampling_mode == SamplingManager.MOSTPROBABLE
                IDs = obj.MostProbableTesting(Agents,M,excluded,t);
            elseif obj.sampling_mode == SamplingManager.MIN_PROJENTROPY
                IDs = obj.minimumPostEntropyTesting(Agents,M,excluded,t);
            elseif obj.sampling_mode == SamplingManager.MAX_PROJPROB
                IDs = obj.maxProjectedProbabilityTesting(Agents,M,excluded,t);
            elseif obj.sampling_mode == SamplingManager.PAGERANK
                IDs=obj.centralityTesting(Agents,M,excluded,'pagerank',false,t,false);
            elseif obj.sampling_mode == SamplingManager.PAGERANK_PROB
                IDs=obj.centralityTesting(Agents,M,excluded,'pagerank',false,t,true);
            elseif obj.sampling_mode == SamplingManager.CLOSENESS
                IDs=obj.centralityTesting(Agents,M,excluded,'closeness',true,t,false);
            elseif obj.sampling_mode == SamplingManager.BETWEENESS
                IDs=obj.centralityTesting(Agents,M,excluded,'betweenness',true,t,false);   
            elseif obj.sampling_mode == SamplingManager.KEMENY
                IDs=obj.kemenyTesting(Agents,M,excluded,t);   
            elseif obj.sampling_mode == SamplingManager.MAX_EIGENVALUE
                IDs=obj.centralityTesting(Agents,M,excluded,'eigenvector',false,t,false);
            elseif obj.sampling_mode == SamplingManager.WEIGHTED_PROB
                IDs=obj.weightedProbTesting(Agents,M,excluded,t);
            else 
                IDs = [];
            end
        end
        
        
        function [IDs,outcomes] = testAgents(obj,Agents,M,excluded,t) % time in days
              
            IDs = obj.getTestIDs(Agents,M,excluded,t);
             
            if isempty(IDs) 
                outcomes=[]; 
                return;
            end
            
            outcomes = NaN(size(IDs)); 
            
            for i=1:numel(IDs) 
                if Agents{IDs(i)}.test(t) 
                    outcomes(i) = SamplingManager.OUTCOME_POSITIVE;
                else
                    outcomes(i) = SamplingManager.OUTCOME_NEGATIVE;
                end
            end 
             
        end
 
%% Phase estimation        
        
        function Pe = projectPhases(obj,Agents,tested,outcomes)
            global N
            
            Pe=obj.Phases;
            IDs = setdiff((1:N)',tested);
            
            for j=1:numel(tested) 
                
                % update not tested
                for i=1:numel(IDs)

                    cp = obj.conditionalProbability(Agents,IDs(i),tested(j));

                    if outcomes(j)==SamplingManager.OUTCOME_POSITIVE
                        
                        Pe(IDs(i)) = max(Pe(IDs(i)),cp);
                        
                    elseif outcomes(j)==SamplingManager.OUTCOME_NEGATIVE
                         
                        np = (Pe(IDs(i))-cp*Pe(tested(j)))*pinv(1-Pe(tested(j)));
                        Pe(IDs(i)) = min( Pe(IDs(i)), max(0,np) ); 
                        
                    end
                    
                end
                
                % update tested
                if outcomes(j)== SamplingManager.OUTCOME_POSITIVE
                    Pe(tested(j)) = 1;
                elseif outcomes(j)== SamplingManager.OUTCOME_NEGATIVE
                    Pe(tested(j)) = 0;
                end
                
            end
             
            
        end
        
        function Pe = updatePhaseEstimates(obj,Agents,tested,outcomes)
            Pe = obj.projectPhases(Agents,tested,outcomes);
            obj.Phases = Pe;  
        end
        
 %% Probabilities       
 
        function cp = conditionalProbability(obj,Agents,i,j) 
            v = Agents{i}.getContactValue(j);   
            cp = obj.cpf(obj.Phases(i),v);
        end
            
   
 
        
    end
end
