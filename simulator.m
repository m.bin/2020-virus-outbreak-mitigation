%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Simulator.m: performs the simulation of the outbreak 
%   with testing and vaccination
%   Michelangelo Bin & Thomas Parisini
%   24 November 2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This file is stand-alone executable, it can be executed directly
% performing a simulation with standard (but modifiable) settings.

% clear all;
close all; 

%% 1. Basic parameters:
global N

% population size
N = 1000;        

% auxiliariy functions
global P ifelse

P = @(probability) rand()<= probability;
ifelse = @(cond,true,false) true*cond+false*(1-cond);
gammadist = @(x,a,b) b^a.*x.^(a-1).*exp(-b*x)/gamma(a); % mean = a/b
gaussdist = @(x,a,b) exp(-(1/2)*((x-a)./b).^2)./(b*sqrt(2*pi)); % mean a


% basic settings
SPREAD_DISEASE= true;           % enables/disables the virus outbreak
PLOT_GRAPH = false;             % enables/disables plot of daily interaction graph
TESTING_ACTIVATED = false;      % enables/disables testing
DEBUG = true;                   % enables/disables debug
MINIMALIST_SIM = true;          % enables/disables minimalist plotting
VACCINATION_ACTVATED = false;   % enables/disables vaccination
 
COMPARE_MODE=false;             % enables/disables compare mode (see section below)
SAVE_WORKSPACE=false;           % enables/disables workspace saving


%% 2. Network parameters

% Bubbles are special sets of individuals corresponding to a decreasing
% level of interacion (e.g. household, co-workers, friends). With each
% individual i we associate three bubbles containing the individuals with
% which i interacts the most.

% Bubbles maximum sizes are drawn by gamma distributions as follows
distrib_span = 1:1:25;
 
BUB_maxsize_distributions = {
    Distribution1D(@(x) gammadist(x,3,3/2),distrib_span),...   % Bub1
    Distribution1D(@(x) gammadist(x,2,2/6),distrib_span),...   % Bub2
    Distribution1D(@(x) gammadist(x,2,2/8),distrib_span),...   % Bub3
};     

% uncomment this to plot distributions 
%  BUB_maxsize_distributions{1}.plot(100);figure(100);hold on;
%  BUB_maxsize_distributions{2}.plot(100);figure(100);hold on;
%  BUB_maxsize_distributions{3}.plot(100); 
 
% total number of bubbles for each individual
N_BUB = length(BUB_maxsize_distributions);

% The elements of each bubble have different probability to be added each day.
% Edge probabailities are drawn by distributions as follows. 
% Time dependency is used to model week periodicity.
BUB_probability = {
    @(t) ifelse(arrayContains([0;4],mod(t,7)),.8,.6),...   % with Bub1 always high probability
    @(t) ifelse(arrayContains([0;4],mod(t,7)),.7,.2),...   % with Bub2 is it more likely on the week days
    @(t) ifelse(arrayContains([0;4],mod(t,7)),.3,.5),...   % with Bub3 is it more likely on the weekend  
}; 

% All the other individuals not belonging to any of the bubbles have the following edge probability 
baseline_edge_p = 10/N;


%% 3. Contacts Parameters 
global CONTACT_ERASE_THRESHOLD CONTACT_FORGETTING_FACTOR sigma sigma_inv

% The intensity of contacts varies depending to which bubble the individuals belong to
% the strength of contacts is drawn by distributions defined as follows
distrib_span = 0:1e-3:1;
 
BUB_weight_probability = {
    Distribution1D(@(x) gammadist(x,3,3/.7),distrib_span),...   % Bub1
    Distribution1D(@(x) gammadist(x,2,2/.6),distrib_span),...   % Bub2
    Distribution1D(@(x) gammadist(x,1,1/.4),distrib_span),...   % Bub3
};

% uncomment this to plot distributions 
%  BUB_weight_probability{1}.plot(101);figure(101);hold on;
%  BUB_weight_probability{2}.plot(101);figure(101);hold on;
%  BUB_weight_probability{3}.plot(101);
  
% All the other individuals not belonging to any of the bubbles have the following strength distribution 
baseline_edge_w = Distribution1D(@(x) gammadist(x,1,1/.01),distrib_span);
 
% contact values map sigma and inverse
a=2;
sigma = @(x) 2./(1+exp(-a*x))-1; 
sigma_inv=@(y) -(1/a).*log(-1+2./(y+1));
 
% contacts forgetting factor 
CONTACT_ERASE_THRESHOLD=0.05; % threshold under which
CONTACT_FORGETTING_FACTOR  = (CONTACT_ERASE_THRESHOLD^(1/15)); % contact forgetting factor
 

%% 4. Infection Spread Parameters
 
% Incubation and resolving time distributions
distrib_incubation_time = @(x) gammadist(x,7,7/4);
distrib_resolve_time = @(x) gammadist(x,20,20/15);
distrib_span = 0:1:40;  
p_distrib_incubation_time = Distribution1D(distrib_incubation_time,distrib_span);
p_distrib_resolve_time = Distribution1D(distrib_resolve_time,distrib_span);

% uncomment this to plot distributions 
%  p_distrib_incubation_time.plot(102);figure(102);hold on;
%  p_distrib_resolve_time.plot(102);  

% Percentage and number of initial infected
p_init_infected = 1*1e-2; 
initial_infected = max(1,round(N*p_init_infected));  

% Infection probability for a maximal contact
% given a contact of strength w, the probability of infection is 
% max_inf_p*w
max_inf_p = 1/4; 

%% 5. Phase Estimation Parameters

% Initial value of the phase estimates
INITIAL_IFECTION_P = p_init_infected;

% Interpolation function giving the estimate of the conditional probability
% p(i|j) given the value of interaction between i and j
conditionalProbabilityEstimationFunction = @(pi,v) pi+v*(1-pi); 


%% 6. Measurement Selection Parameters
global TESTING_REFRACTORY_PERIOD

% Default sampling mode for phase 1
MEASURES_SELECTION_SAMPLING_MODE = SamplingManager.NOTESTING;

% Default sampling mode for phase 2
MEASURES_SELECTION_ISOLATION = SamplingManager.NOTESTING;

% number of daily tests
DAILY_TESTS = round(N*0.05);

% Percentage dedicated to phase 1
PERC_TESTS_SELECTION = .9;

% Daily maximum vaccines
DAILY_VACCINES = 5;

% Testing refractory period: an individual cannot be tested twice within
% each interval of length TESTING_REFRACTORY_PERIOD
TESTING_REFRACTORY_PERIOD = 3;


%% 7. Quarantine Parameters
global QUARANTINE_TIME

% Quarantine duration (in days)
QUARANTINE_TIME = 14;  
 

%% 8. External Calls

% This is used to call the script from other files. The calling script will
% override the default parameters by properly redefining the corresponding
% variables in the .mat file SETTINGS_FILE and will set
% EXTERNAL_CALL=true

try
    if EXTERNAL_CALL
   
        load(SETTINGS_FILE);
    
    end
catch e
end


%% 9. Initialisation: Agents

fprintf('initialization...');

% Initialisation of individuals. The cell array Agents contains references
% to all individuals
Agents = cell(N,1);      
  
for i=1:N    
    Agents{i} =  Agent(...
                i,... 
                p_distrib_incubation_time.sample(1),...
                p_distrib_resolve_time.sample(1),... 
                zeros(N_BUB,1)...
             ); 
end



%% 10. Blubbles Creation

for k=1:N_BUB
     
    processed=[];

    % until every individual has been processed...
    while numel(processed)<N
        
        % Feasible individuals are those still to be processed
        Feas = setdiff((1:N)',processed);
        
        % Draw a random size for the bubble and initialise it
        h_size = min(BUB_maxsize_distributions{k}.sample(1),numel(Feas));
        bub = nan(h_size,1);
   
        % This will add to the bubble feasible individuals.
        % In order to force disjointness of blubbles individuals that are 
        % not in the previous bubbles will be added
        j=1;
        while j<=h_size && numel(Feas)>0 
            
            % Pick a feasible individual randomly
            idx = 1 + floor( rand()*numel(Feas) );
            
            % Check if it belongs to previous bubbles of
            % individuals already in bub
            already_in_bubbles = false;
            for p=1:j-1
                if Agents{bub(p)}.getContainingBubble(Feas(idx))>0
                    % in this case, Feas(idx) belongs to a bubble of bub(p)
                    already_in_bubbles=true;
                    break;
                end
            end
            
            % if Feas(idx) does no belong to previous bubbless add it to bub 
            if ~already_in_bubbles
                bub(j) = Feas(idx);
                j=j+1;
            end
            
            % remove Feas(idx) from feasibles
            Feas(idx)=[];
            
        end
        
        % Refine bubble
        bub = rmmissing(bub);
        
        % Assign to each j in bub the bubble bub/{j}
        for j=bub(:)'
            Agents{j}.setBubble(setdiff(bub,j),k); 
        end 
        
        % add the elements of bub to processed
        processed=[processed;bub(:)];
    end
    
end

 
% This is a check that all bubbles satisfy the properties of
%   - being disjoint for each individual
%   - being equivalence classes
% it also computes and shows the average size of bubbles

bub_avg = zeros(N_BUB,1);
for i=1:N
    for k=1:N_BUB
        Bik = Agents{i}.getBubble(k);
        bub_avg(k) = bub_avg(k)+(numel(Bik)+1)/N;
        for j=Bik(:)'
           if ~isequal(sort(setdiff(Bik(:),j)),sort(setdiff(Agents{j}.getBubble(k),i)))
               error('Bubbles error: bubble %d of individual %d does not match with that of %d',k,i,j);
           end
        end
    end
end

% Print average bubbles size
fprintf('\n Average bubbles size');
for k=1:N_BUB
   fprintf('\n\t - %d = %2.2f',k,bub_avg(k)); 
end
fprintf('\ncompleted');


%% 11. Initialization of DataObserver and SamplingManager
 
% The DataObserver module is used to keep track of statistics about the
% network and infection, such as the average number and strength of
% contacts, or the number of infected individuals

% The SamplingManager module, instead, contains the phases estimate and all
% the methods for testing

% Initialize SystemObserver
SO = SystemObserver();

fprintf('\nInitially infected:');

% Infect initially agents and add them to SO
for j=1:initial_infected
    i = ceil(rand()*N);
    Agents{i}.setInfected(0); 
    SO.addInfected(i); 
    fprintf('%d,',i);
end 
 
 
% Initialize Sampling Manager
SM = SamplingManager(INITIAL_IFECTION_P*ones(N,1), ...
                     conditionalProbabilityEstimationFunction);

                 
%% 12. Compare Mode

if ~COMPARE_MODE && SAVE_WORKSPACE
    save('workspace');
end

if COMPARE_MODE
  clear all; 
  load('workspace');
  
 % In compare mode, all the variables will be overwritten by what is inside the (previously saved)
 % file 'workspace.mat'. Here modifications to the settings can be done
   
end 
 
fprintf('\nInitialisation completed');


%% 13. Simulation Parameters

% initial time
T0 = 1;
% final time 
Tf = 7*13;      
% Time domain
Time = (T0:Tf)'; 

fprintf('\nSimulating %d days',Tf-T0+1);
  
%% 14. Creating the Axes

fprintf('\nCreating axes...');

% Standard Colormap
COL_S = [0 0 .7];       % Susceptible 
COL_E = [1 1 0];        % Exposed 
COL_I = [1 0 0];        % Infected
COL_R = .6*[.5 1 .5];   % Resolved
COL_V = [.5 .1 .5];     % Vaccined
COL_Q = .6*[1 1 1];     % Quarantined
COL_eI = [0 0 0];       % Cumulative Infected

% Creating the main figures
fig1=figure('units','normalized','position',[.05 .3 .9 .6]);

ax=axes('position',[0.05 0.1 .45 .8]);
box(ax,'on');

ax2=axes('xlim',[0 Tf],'ylim',[0 N],'position',[0.55 0.5 .25 .4]);
title(ax2,'number of infected');
box(ax2,'on');

ax3=axes('xlim',[0 Tf],'ylim',[0 N],'position',[0.55 0.1 .18 .3]);
box(ax3,'on');

ax4=axes('xlim',[0 Tf],'ylim',[0 N],'position',[0.77 0.1 .18 .3]);
box(ax4,'on');    

title(ax3,'number of contacts per agent');
title(ax4,'intensity of contacts');

ax5=axes('xlim',[0 Tf],'ylim',[0 10],'position',[0.825 0.5 .125 .4]);
title(ax5,'R0');
box(ax5,'on');

% Creating the sampling figure
fig2=figure('units','normalized','position',[.05 .07 .9 .4]);

ax21=axes('xlim',[0 Tf],'ylim',[0 1],'position',[0.025 0.15 .3 .7]);
box(ax21,'on');

ax22=axes('xlim',[0 Tf],'ylim',[0 1],'position',[0.35 0.15 .3 .7]);
box(ax22,'on');
 
ax23=axes('xlim',[0 Tf],'ylim',[0 1],'position',[0.675 0.15 .3 .7]);
box(ax23,'on');

fprintf(' completed');

%% 14. Initialization of the Simulation Signals

fprintf('\nSimulation started');

% Infection stats timeseries
nS = NaN(length(1:Tf),1);           % Susceptible
nE = NaN(length(1:Tf),1);           % Exposed
nI = NaN(length(1:Tf),1);           % Infected
nR = NaN(length(1:Tf),1);           % Resolved
nV = NaN(length(1:Tf),1);           % Vaccined
nQ = NaN(length(1:Tf),1);           % Quarantined
R0 = NaN(length(1:Tf),1);           % Average number of secondary infections
everI = NaN(length(1:Tf),1);        % Cumulative Infected

% Contact properties stats timeseries  
avg_n = NaN(length(1:Tf),1);        % Average number of contacts per individual
max_n = NaN(length(1:Tf),1);        % Max number of contacts per individual
med_n = NaN(length(1:Tf),1);        % Median number of contacts per individual
avg_v = NaN(length(1:Tf),1);        % Average value of contacts per individual
max_v = NaN(length(1:Tf),1);        % Max value of contacts per individual
med_v = NaN(length(1:Tf),1);        % Median value of contacts per individual
 
% Testing stats timeseries
n_daily_positive_tests = NaN(length(1:Tf),1);   % Number of daily positives
n_daily_tested = NaN(length(1:Tf),1);           % Number of daily tests
n_daily_quarantined = NaN(length(1:Tf),1);      % Number of daily quarantines
n_daily_vaccinated = NaN(length(1:Tf),1);       % Number of daily vaccinated
ratio_positive_tests = NaN(length(1:Tf),1);     % Ratio daily positives/tested

% Phase estimate timeseries
Phases = NaN(length(1:Tf),N);

% Debug signal
db_bub_inst = zeros(N_BUB+1,1);
  
%% 15. Simulation

for t=Time'
    
fprintf('\nDay %d:',t);
     
% DAILY GRAPH: this creates the daily interaction network and spreads the
% infection
    
    fprintf('\n - generating daily graph');
     
    infection_edges = [];
    edges = zeros(N,N); 
    
    for i=1:N
        
        % update infection and contact state
        Agents{i}.update(t); 
        
        % if agent i is in quarantine skip
        if Agents{i}.isQuarantined()  
        	continue;
        end
        
        % add edges and infect people
        for j=i+1:N
             
            % if agent j is in quarantine skip
            if Agents{j}.isQuarantined()  
                continue;
            end
            
            % retrieve the bubble in which j is
            k = Agents{i}.getContainingBubble(j);  
            
            % if k<0 means that j is not in a bubble of i
            if k<0
                p = baseline_edge_p;
                w = baseline_edge_w.sample(1);
            else
                p = BUB_probability{k}(t);
                w = BUB_weight_probability{k}.sample(1);
            end  
            
            % with probability p add an edge with wieght w. 
            % Then spread the virus.
            if P(p)
                  
                Agents{i}.newContact(j,w,t);
                Agents{j}.newContact(i,w,t); 
                
                edges(i,j) = w;
                edges(j,i) = w;
                
                % infect 
                if SPREAD_DISEASE
                    inf_p = max_inf_p*w; 
                    flag = false;
                    if P(inf_p)
                        if Agents{i}.isContagious() && Agents{j}.isSusceptible() 
                            Agents{j}.setExposed(t);
                            Agents{i}.newPersonInfected();
                            if PLOT_GRAPH
                                infection_edges = [infection_edges;[i j]]; 
                            end
                            flag=true;
                        elseif Agents{j}.isContagious() && Agents{i}.isSusceptible() 
                            Agents{i}.setExposed(t);
                            Agents{j}.newPersonInfected();
                            if PLOT_GRAPH
                                infection_edges = [infection_edges;[j i]];
                            end
                            flag=true;
                        end
                        if DEBUG && flag
                            db_i =  ifelse(k<0,N_BUB+1,k);
                            db_bub_inst(db_i) = db_bub_inst(db_i)+1;
                        end
                    end
                end
                      
            end 
            
        end 
        
    end
    
    if ~MINIMALIST_SIM
        dailyG = graph(edges,string(1:N));
    end
     
    
% TESTING:  here is wher testing is done
      
    if TESTING_ACTIVATED  
        fprintf('\n testing:');
        
        % Phase 1: measurement selection testing
        % exclude the people we know the outcome already 
        toExclude = unique([SO.V;SO.Q]);
        % compute the number of tests for phase 1
        M_MEAS_SEL = min(round(PERC_TESTS_SELECTION*DAILY_TESTS),N-length(toExclude)); 
        
        % Sample and Test
        SM.setSamplingMode(MEASURES_SELECTION_SAMPLING_MODE);
        [tested1,outcomes1] = SM.testAgents(Agents,M_MEAS_SEL,toExclude,t);
      
        fprintf('\n  Phase 1, mode: %s, M: %d',...
           SM.TEST_MODE_STRINGS(1+MEASURES_SELECTION_SAMPLING_MODE),...
           M_MEAS_SEL...
        );
        
        % Apply quarantine where needed
        positives1 = tested1(outcomes1==SamplingManager.OUTCOME_POSITIVE);
        quarantined1 = SamplingManager.quarantineAgents(positives1,Agents,t);
        
        % Update phase estimation
        Phases(t,:) = SM.updatePhaseEstimates(Agents,tested1,outcomes1)';
        
        % Phase 2: mitigation testing
        % if we have vaccines, then we may want to vaccinate also people
        % already tested in Phase 1, otherwise we take them off
        if VACCINATION_ACTVATED
        	addtoexclude = positives1;
        else
            addtoexclude = tested1;
        end
        toExclude = unique([SO.V;SO.Q;addtoexclude]);
        
        % compute number of tests
        M_ISOL = min(DAILY_TESTS-M_MEAS_SEL,N-length(toExclude)); 
        
        % Test and isolate
        SM.setSamplingMode(MEASURES_SELECTION_ISOLATION);
        [tested2,outcomes2] = SM.testAgents(Agents,M_ISOL,toExclude,t);
         
        fprintf('\n  Phase 2, mode: %s, M: %d, v: %d',...
            SM.TEST_MODE_STRINGS(1+MEASURES_SELECTION_ISOLATION),...
            M_ISOL,DAILY_VACCINES...
        );
        
        positives2 = tested2(outcomes2==SamplingManager.OUTCOME_POSITIVE);
        
        % Apply vaccination or quarantine
        if VACCINATION_ACTVATED
            [vaccinated,quarantined2] = SamplingManager.vaccinateOrQuarantineAgents(tested2,Agents,t,DAILY_VACCINES);
        else
            vaccinated=[];
            quarantined2 = SamplingManager.quarantineAgents(positives2,Agents,t);
        end
        
        % Update phase estimation
        Phases(t,:) = SM.updatePhaseEstimates(Agents,tested2,outcomes2)';
        
        % Update timeseries and visualisation
        tested = [tested1;tested2];
        outcomes = [outcomes1;outcomes2];
        positives = [positives1;positives2];
        quarantined = [quarantined1;quarantined2];
        
        % Checks
        if numel(tested1)~=  M_MEAS_SEL 
            warning('\n  error: phase 1 - fewer people tested - %d - %d',numel(tested1),M_MEAS_SEL);   
        end
        if numel(tested2)~= M_ISOL
            warning('\n  error: phase 2 - fewer people tested - %d - %d',numel(tested2),M_ISOL);
        end
        if numel(tested1)~=numel(unique(rmmissing(tested1)))
            warning('\n  error: phase 1 - measurement selection procedure failed');
        end
        if numel(tested2)~=numel(unique(rmmissing(tested2)))
            warning('\n  error: phase 2 - measurement selection procedure failed');
        end
        
        % Update stats
        n_daily_positive_tests(t) =  numel(positives);
        n_daily_tested(t) = numel(tested);
        ratio_positive_tests(t) =  n_daily_positive_tests(t)*pinv(n_daily_tested(t));
        n_daily_quarantined(t) = numel(quarantined);
        n_daily_vaccinated(t) = numel(vaccinated);
        
        fprintf('\n  Today positive: %d [%2.2f]\n  today quarantined: %d',n_daily_positive_tests(t),ratio_positive_tests(t),n_daily_quarantined(t));
        if VACCINATION_ACTVATED
            fprintf('\n  Today vaccinated: %d',n_daily_vaccinated(t));
        end
    end
    
   
% STATS: here is where stats are computed and plotted
    fprintf('\n - Updating stats');
    
    % Update contact\infection stats
    SO.updateInfectionStats(Agents);
    
    nS(t)   = SO.getNumberSusceptible();
    nE(t)   = SO.getNumberExposed();
    nI(t)   = SO.getNumberInfected(); 
    nR(t)   = SO.getNumberResolved();
    nV(t)   = SO.getNumberVaccinated();
    nQ(t)   = SO.getNumberQuarantined();
    everI(t)= SO.getNumberEverInfected();
    R0(t)   = SO.getR0();
    
    if ~MINIMALIST_SIM
        SO.updateContactStats(Agents); 
        
        avg_n(t) = SO.mean_number_of_contacts;
        max_n(t) = SO.max_number_of_contacts;
        med_n(t) = SO.median_number_of_contacts;
        avg_v(t) = SO.mean_value_of_contacts;
        max_v(t) = SO.max_value_of_contacts;
        med_v(t) = SO.median_value_of_contacts;
    end
    
          

% PLOT: plot timeseries
     
        
    % 1) Plot interaction graph
    cla(ax); 
    title(ax,'');
    title(ax,sprintf('day %d',t));

    if PLOT_GRAPH && ~MINIMALIST_SIM

        fprintf('\n\t plotting graph');
        LWidths = 3*dailyG.Edges.Weight/max(dailyG.Edges.Weight);
        hold(ax,'on');
        h=plot(ax,dailyG,'LineWidth',LWidths); 
        highlight(h,SO.S,'NodeColor',COL_S);
        highlight(h,SO.E,'NodeColor',COL_E);
        highlight(h,SO.I,'NodeColor',COL_I);
        highlight(h,SO.R,'NodeColor',COL_R);
        highlight(h,SO.V,'NodeColor',COL_V);
        highlight(h,SO.Q,'NodeColor',COL_Q);

        if ~isempty(infection_edges)
            highlight(h,string(infection_edges(:,1)),...
                string(infection_edges(:,2)),...
                'EdgeColor','r');
        end

        h.MarkerSize = 3;

    end


    % 2) Plot infection timeseries
    fprintf('\n\t plotting timeseries');
    cla(ax2);
    title(ax2,'');

    set(ax2,'ColorOrder',[COL_S;COL_E;COL_I;COL_R;COL_V;COL_Q;COL_eI]);
    hold(ax2,'on');
    plot(ax2,Time,nS,Time,nE,Time,nI,Time,nR,Time,nV,Time,nQ,Time,everI,'linewidth',2);

    title(ax2,'');
    legend(ax2,'S','E','I','R','V','q','eI','location','northwest');
    ylim(ax2,[0,N]);
    xlim(ax2,[0,t+2]);
    title(ax2,sprintf('%d S, %d E, %d I, %d R, %d V, %d q, %d eI',nS(t),nE(t),nI(t),nR(t),nV(t),nQ(t),everI(t)));
 


    % 3) Plot contact stats
    if ~MINIMALIST_SIM
        fprintf('\n\t plotting contact stats');
        cla(ax3);
        cla(ax4);
        plot(ax3,Time,avg_n,Time,max_n,Time,med_n,'linewidth',2);
        legend(ax3,'mean','max','median','location','northwest');
        plot(ax4,Time,avg_v,Time,max_v,Time,med_v,'linewidth',2);
        legend(ax4,'mean','max','median','location','northwest');
        xlim(ax3,[0,t+2]);
        xlim(ax4,[0,t+2]);
        title(ax3,'number of contacts per agent');
        title(ax4,'intensity of contacts'); 
    end
    
    cla(ax5);
    plot(ax5,Time,R0,'linewidth',2); 
    title(ax5,'R_{0}');
    xlim(ax5,[0,t+2]);

    % 4) Plot probabilities
    if TESTING_ACTIVATED && (DEBUG || ~MINIMALIST_SIM)
    
        fprintf('\n\t plotting estimates');
        PE = Phases(t,:)';
        
        SnQ = setdiff(SO.S,SO.Q);
        SQ  = setdiff(SO.S,SnQ);
        EnQ = setdiff(SO.E,SO.Q);
        EQ  = setdiff(SO.E,EnQ);
        InQ = setdiff(SO.I,SO.Q);
        IQ  = setdiff(SO.I,InQ);
        RnQ = setdiff(SO.R,SO.Q);
        RQ  = setdiff(SO.R,RnQ);
        VnQ = setdiff(SO.V,SO.Q);
        VQ  = setdiff(SO.V,VnQ);
        
        cla(ax21);
        if ~isempty(SnQ)
            stem(ax21,SnQ,log(1+PE(SnQ)),'markersize',2,'markerfacecolor',COL_S,'color',COL_S);
            hold(ax21,'on');
        end
        %     if ~isempty(SQ)
        %         stem(ax21,SQ,PE(SQ),'markersize',2,'markerfacecolor',COL_S,'markeredgecolor',[0 0 0]);
        %         hold(ax21,'on');
        %     end
        if ~isempty(EnQ)
            stem(ax21,EnQ,log(1+PE(EnQ)),'markersize',2,'markerfacecolor',COL_E,'color',COL_E);
            hold(ax21,'on');
        end
        %     if ~isempty(EQ)
        %         stem(ax21,EQ,PE(EQ),'markersize',2,'markerfacecolor',COL_E,'markeredgecolor',[0 0 0]);
        %         hold(ax21,'on');
        %     end
        if ~isempty(InQ)
            stem(ax21,InQ,log(1+PE(InQ)),'markersize',2,'markerfacecolor',COL_I,'color',COL_I);
            hold(ax21,'on');
        end
        %     if ~isempty(IQ)
        %         stem(ax21,IQ,PE(IQ),'markersize',2,'markerfacecolor',COL_I,'markeredgecolor',[0 0 0]);
        %         hold(ax21,'on');
        %     end
        if ~isempty(RnQ)
            stem(ax21,RnQ,log(1+PE(RnQ)),'markersize',2,'markerfacecolor',COL_R,'color',COL_R);
            hold(ax21,'on');
        end
        %     if ~isempty(RQ)
        %         stem(ax21,RQ,PE(RQ),'markersize',2,'markerfacecolor',COL_R,'markeredgecolor',[0 0 0]);
        %         hold(ax21,'on');
        %     end
        if ~isempty(VnQ)
            stem(ax21,VnQ,log(1+PE(VnQ)),'markersize',2,'markerfacecolor',COL_V,'color',COL_V);
            hold(ax21,'on');
        end
        %     if ~isempty(VQ)
        %         stem(ax21,VQ,PE(VQ),'markersize',2,'markerfacecolor',COL_V,'markeredgecolor',[0 0 0]);
        %         hold(ax21,'on');
        %     end
        hold(ax21,'off'); 
    end

    % 5) Plot testing stats
    if ~DEBUG 
        if ~MINIMALIST_SIM
            cla(ax22);
            plot(ax22,Time,n_daily_tested,Time,n_daily_positive_tests,Time,n_daily_quarantined,'linewidth',2);
            xlim(ax22,[0,t+2]);
            title(ax22,sprintf('today tested positive: %d over %d tests',n_daily_positive_tests(t),n_daily_tested(t)));
            legend(ax22,'tested','positive','quarantined','location','northwest');
            %
            plot(ax23,Time,n_daily_quarantined./nI,'linewidth',2);
            title(ax23,sprintf('ratio q/positive: %2.3f',n_daily_quarantined(t)/nI(t)));
            xlim(ax23,[0,t+2]);
        end
    else
        cla(ax22);
        stem(ax22,[1:N_BUB,-1]',db_bub_inst);
    end

pause(1e-3); 
      
end



 