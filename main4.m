%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   main*.m: performs repeated simulations of the outbreak 
%   with different testing and vaccination strategies
%   Michelangelo Bin & Thomas Parisini
%   24 November 2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Although simulator.m is stand-alone, using the main*.m files to perform
% multiple or repeated simulations is more convenient as this feature 
% is not supported by simulator.m 

% Each main*.m file contains a subset of simulations. This is done to
% enforce a naive parallelisation (the code has not been written for 
% parallelisation yet) as in this way multiple main*.m can be
% executed at the same time. 

close all
clear all

%% 1. Simulator Settings

% In the following some of the default settings of simulator.m are
% overwritten. Then EXTERNAL_CALL = true is enforced so as when simulator.m
% is called it will be use the new variables values.

SPREAD_DISEASE= true;
PLOT_GRAPH = false;
PLOT_GRAPH_IDS = false;
TESTING_ACTIVATED = true;
DEBUG = true;
MINIMALIST_SIM = true; 
VACCINATION_ACTVATED = true; 
COMPARE_MODE=false;
SAVE_WORKSPACE=false; 

N=1000;
DAILY_TESTS = 70; 
DAILY_VACCINES = 5;

EXTERNAL_CALL = true;

%% 2. Output Settings

FILE_NUMBER = 4;
DATA_PATH = './data';
SETTINGS_PATH = strcat(DATA_PATH,'/temp');
DATA_FNAME = sprintf('simout_%d',FILE_NUMBER);
SETTINGS_FNAME = sprintf('sim_settings_%d',FILE_NUMBER);

%% 3. Simulations Settings

% The cell array EXT_simulation_data contains the data of the repeated
% simulations to perform. 
% The template is as follows:
%   EXT_simulation_data = {
%       Entry_1
%       Entry_2
%       ...
%       Entry_n
%   };
% in which n is arbitrary and Entry_k has the form
%   Entry_k = [MODE1 MODE2 perc1 number_of_sim]
% in which 
%   - MODE1 = testing strategy for phase 1  
%   - MODE2 = testing strategy for phase 2 
%   - perc1 = percentage of DAILY_TESTS for phase 1 
%   - number_of_sim = repetitions with these settings
%
% Example:
% EXT_simulation_data = {
%   [SamplingManager.NOTESTING SamplingManager.NOTESTING 0 10]
%   [SamplingManager.RANDOM SamplingManager.RANDOM .9 10]
%   [SamplingManager.PAGERANK SamplingManager.MOSTPROBABLE .9 10] 
%   [SamplingManager.PAGERANK SamplingManager.MAX_PROJPROB .9 10]
% };


% Set simulation data
EXT_simulation_data = { 
  [SamplingManager.MIN_PROJENTROPY SamplingManager.MAX_PROJPROB 50/70 100]
}; 
 
EXT_M = numel(EXT_simulation_data);
EXT_output = cell(EXT_M,1);

% settings file
SETTINGS_FILE = strcat(SETTINGS_PATH,'/',SETTINGS_FNAME);

% Execute simulations
for EXT_i=1:EXT_M
    
     EXT_output{EXT_i} = cell(EXT_simulation_data{EXT_i}(4),1);
    
    for EXT_n=1:EXT_simulation_data{EXT_i}(4)
     
        MEASURES_SELECTION_SAMPLING_MODE = EXT_simulation_data{EXT_i}(1);
        MEASURES_SELECTION_ISOLATION = EXT_simulation_data{EXT_i}(2); 
        PERC_TESTS_SELECTION = EXT_simulation_data{EXT_i}(3);
        
        if EXT_simulation_data{EXT_i}(1)==SamplingManager.NOTESTING && EXT_simulation_data{EXT_i}(2)==SamplingManager.NOTESTING
            TESTING_ACTIVATED = false;
        else
            TESTING_ACTIVATED = true;
        end
        
        save(SETTINGS_FILE);
    
        fprintf('\n\nExecuting sim %d (%d/%d) with\n phase 1: %d\n phase 2: %d\n perc: %2.2f\n\n',...
            EXT_i,EXT_n,EXT_simulation_data{EXT_i}(4),EXT_simulation_data{EXT_i}(1),EXT_simulation_data{EXT_i}(2),EXT_simulation_data{EXT_i}(3));
        
       
        simulator
        
        EXT_output{EXT_i}{EXT_n}={
            Time;nS;nE;nI;nR;nV;nQ;everI;...
            R0;db_bub_inst;...
            avg_n;max_n;med_n;avg_v;max_v;med_v;...
            n_daily_quarantined;n_daily_positive_tests;n_daily_tested;...
            n_daily_quarantined;n_daily_vaccinated;ratio_positive_tests;...
            Phases
        };
     
    
        clearvars -except SPREAD_DISEASE PLOT_GRAPH TESTING_ACTIVATED DEBUG MINIMALIST_SIM VACCINATION_ACTVATED COMPARE_MODE SAVE_WORKSPACE...
                    N DAILY_TESTS DAILY_VACCINES EXTERNAL_CALL DATA_PATH DATA_FNAME EXT_simulation_data EXT_M EXT_output EXT_i EXT_n SETTINGS_FILE
        
           
        close all
     
    end
end

fprintf('\nSimulations finished. Saving output data...');

save(strcat(DATA_PATH,'/',DATA_FNAME));

fprintf('\n\n Done! Output data saved to %s.\n',strcat(DATA_PATH,'/',DATA_FNAME));

