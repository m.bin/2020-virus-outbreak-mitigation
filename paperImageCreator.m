%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   paperImageCreation.m: generates the paper images 
%   from the data output of main*.m 
%   Michelangelo Bin & Thomas Parisini
%   24 November 2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% This file reads the output data generated by the files main*.m and
% builds images and visualisations from them.
% the data are in /data/simout_*.mat. Backups are in
% /data/backup_paper_data


%% 1. Settings

clear all;
close all;

% Data path
DATA_PATH = './data';

% Files to open
files ={'simout_1','simout_2','simout_3','simout_4','simout_5','simout_6'};
N_FILES = numel(files);

% Color map
COL_S = [0 0 .7];
COL_E = [1 1 0];
COL_I = [1 0 0];
COL_Q = .6*[1 1 1];
COL_R = .6*[.5 1 .5];
COL_V = [.5 .1 .5];
COL_eI = [0 0 0];    

% This is only needed for names of strategies
SM=SamplingManager(false,false);

fprintf('\n Plotting %d simulation clusters',N_FILES);

%% 2. Data retrival and image creation

% Initialise boxplots
BOXPLOT_SETTINGS ={
    {4,"Infected"}
    {8,"Cumulative Infected"} 
    {7,"Quarantined"}
    {6,"Vaccinated"}
};

N_BOXPLOTS = size(BOXPLOT_SETTINGS,1);
BOXPLOT_DATA = cell(N_BOXPLOTS,2);
for b = 1:N_BOXPLOTS
    BOXPLOT_DATA{b}{1}=[];
    BOXPLOT_DATA{b}{2}=[];
end

 
% Initialise timeseries plots
ts_figure = figure('units','inches','position',[1 1 8 6]); 
Axes_I=subplot(2,1,1,'parent',ts_figure);
box(Axes_I,'on');
Axes_cI=subplot(2,1,2,'parent',ts_figure);
box(Axes_cI,'on');
xlabel(Axes_cI,'Time (days)');
title(Axes_I,'Infected');
title(Axes_cI,'Cumulative Infected');

% Settings
COLOR_SCHEME = 255\[[163 0 3];
                [245 86 42];
                [54 78 89];
                [250 190 70];
                [83 153 135];
                [40 175 175];];
c_idx=1;

legend_v = [];
maxT = 0;

% Subset of simulations to plot
TOPLOT = [[SamplingManager.NOTESTING SamplingManager.NOTESTING];
          [SamplingManager.RANDOM SamplingManager.RANDOM];
          [SamplingManager.PAGERANK SamplingManager.MAX_PROJPROB]];
      
% For each file...
for s=1:N_FILES
    
    fprintf('\n Cluster %d:\n\t file: %s',s,strcat(DATA_PATH,'/',files{s}));
    
    clearvars -except DATA_PATH files N_FILES COL_S COL_E COL_I COL_Q ...
        COL_R COL_V COL_eI s SM...
        BOXPLOT_SETTINGS N_BOXPLOTS BOXPLOT_DATA...
        ts_figure Axes_I Axes_cI COLOR_SCHEME c_idx legend_v TOPLOT maxT
    
    load(strcat(DATA_PATH,'/',files{s}));
     
    fprintf('\n\t number of subclusters: %d',EXT_M);
        
    % For each simulation in the file...
    for i=1:EXT_M
        
        fprintf('\n\t subcluster %d\n\t  Phase 1: %s\n\t  - Phase 2: %s',...
            i,SM.TEST_MODE_STRINGS(1+EXT_simulation_data{i}(1)),...
            SM.TEST_MODE_STRINGS(1+EXT_simulation_data{i}(2)));
        
        % Template of EXT_output:
        %   EXT_output{EXT_i}{EXT_n}={
        %             Time;nS;nE;nI;nR;nV;nQ;everI;...
        %             R0;db_bub_inst;...
        %             avg_n;max_n;med_n;avg_v;max_v;med_v;...
        %             n_daily_quarantined;n_daily_positive_tests;n_daily_tested;...
        %             n_daily_quarantined;n_daily_vaccinated;ratio_positive_tests;...
        %             Phases
        %         }; 
         
        
        % Time & number of repetitions
        Time = EXT_output{i}{1}{1};
        Ri = EXT_simulation_data{i}(4);
        maxT = max(maxT,Time(end));
        
        % Plot Timeseries
        if arrayContains(TOPLOT(:,1),EXT_simulation_data{i}(1)) && arrayContains(TOPLOT(:,2),EXT_simulation_data{i}(2))
            
            % Legend and color
            legend_lab = SM.TEST_MODE_ABBREV(EXT_simulation_data{i}(1)+1)+"-"+SM.TEST_MODE_ABBREV(EXT_simulation_data{i}(2)+1);
            color = COLOR_SCHEME(min(c_idx,end),:);
            
            % Computing statistics
            I = zeros(length(Time),Ri);
            cI = I;
            
            for j=1:Ri
                I(:,j) = EXT_output{i}{j}{4}*100/N;
                cI(:,j) = EXT_output{i}{j}{8}*100/N;
            end
            
            m_I = mean(I,2);
            m_cI = mean(cI,2);
            
            q_I = quantile(I',[0.025,0.975])';
            q_cI = quantile(cI',[0.025,0.975])';
            
            % Plot
            hold(Axes_I,'on');
            fill(Axes_I,[Time', fliplr(Time')],[q_I(:,1)', fliplr(q_I(:,2)')],color, 'facecolor', color, 'edgecolor', 'none', 'facealpha', 0.2);
            hold(Axes_I,'on');
            pI=plot(Axes_I,Time,m_I,'linewidth',2,'color',color,'display',legend_lab);
            hold(Axes_cI,'on');
            fill(Axes_cI,[Time', fliplr(Time')],[q_cI(:,1)', fliplr(q_cI(:,2)')],color, 'facecolor', color, 'edgecolor', 'none', 'facealpha', 0.2);
            hold(Axes_cI,'on');
            pcI=plot(Axes_cI,Time,m_cI,'linewidth',2,'color',color,'display',legend_lab);
            
            legend_v = [legend_v;pI pcI];
            
            c_idx = c_idx+1;
        
        end
        
        % Collectiong boxplot data 
        boxp_peaks = nan(Ri,N_BOXPLOTS); 
        
        for j=1:Ri 
            if EXT_simulation_data{i}(1)~=SamplingManager.NOTESTING || EXT_simulation_data{i}(2)~=SamplingManager.NOTESTING
                for b=1:N_BOXPLOTS
                    boxp_peaks(j,b) = max(EXT_output{i}{j}{BOXPLOT_SETTINGS{b}{1}}*100/N);
                end
            end
        end
           
        if EXT_simulation_data{i}(1)~=SamplingManager.NOTESTING || EXT_simulation_data{i}(2)~=SamplingManager.NOTESTING
            boxp_abbrv = SM.TEST_MODE_ABBREV(EXT_simulation_data{i}(1)+1)+"-"+SM.TEST_MODE_ABBREV(EXT_simulation_data{i}(2)+1);
            for b=1:N_BOXPLOTS
                BOXPLOT_DATA{b}{1} = [BOXPLOT_DATA{b}{1};boxp_peaks(:,b)];
                BOXPLOT_DATA{b}{2} = [BOXPLOT_DATA{b}{2};repmat({boxp_abbrv},Ri,1)];
            end
        end 
        
    end
    
end 

legend(Axes_I,legend_v(:,1),'location','northeast');
legend(Axes_cI,legend_v(:,2),'location','northwest');

xlim(Axes_I,[1 maxT]);
xlim(Axes_cI,[1 maxT]);  
xticks(Axes_I,7*[1:2:13]);
xticks(Axes_cI,7*[1:2:13]);
grid(Axes_I,'on');
grid(Axes_cI,'on');  
ytickformat(Axes_I, 'percentage');
ytickformat(Axes_cI, 'percentage');

%% Boxplots

% Settings
BoxPlotFig = figure('units','inches','position',[1 1 10 6]);
BoxPlotAxes = cell(N_BOXPLOTS,1);

cols = 2;
rows = ceil(N_BOXPLOTS/2);

lines_color = .2*[1 1 1];
outliers_color =  lines_color;
median_color = [.4 .6 1]; 
fill_color = .7*[1 1 1];

m = .05;
poss = [[m/2 1/2+m 1/2-1*m 1/2-2*m]; 
        [1/2+m/2 1/2+m 1/2-1*m 1/2-2*m];
        [m/2 m 1/2-1*m 1/2-2*m];
        [1/2+m/2 m 1/2-1*m 1/2-2*m];];

limits = 100*[[0,.5];[0,.8];[0,.5];[0,.5]];

% Creating boxplots
for b=1:N_BOXPLOTS 
    BoxPlotAxes{b} = axes('position',poss(b,:),'parent',BoxPlotFig);%subplot(rows,cols,b,'parent',BoxPlotFig);
    box(BoxPlotAxes{b},'on');
    boxplot(BoxPlotAxes{b},BOXPLOT_DATA{b}{1},BOXPLOT_DATA{b}{2},'plotstyle','traditional','notch',...
        'off','outliersize',6,'symbol','+','Jitter',0,'LabelOrientation','horizontal','colors',lines_color,'width',.4);
    o = findobj(BoxPlotAxes{b},'Type','Line');
    for i=1:length(o)
        set(o(i),'color',lines_color,'linestyle','-');
    end
    o = findobj(BoxPlotAxes{b},'Tag','Median');
    for i=1:length(o)
        set(o(i),'color',median_color,'linewidth',2);
    end
    o = findobj(BoxPlotAxes{b},'Tag','Outliers');
    for i=1:length(o)
        set(o(i),'markeredgecolor',outliers_color,'linestyle','none');
    end
    o = findobj(BoxPlotAxes{b},'Tag','Box');
    for j=1:length(o) 
%         patch(get(o(j),'XData'),get(o(j),'YData'),fill_color,'FaceAlpha',.1,'linestyle','none','marker','none'); 
        hold(BoxPlotAxes{b},'on');
        fill(get(o(j),'XData'),get(o(j),'YData'),fill_color,'facecolor', fill_color, 'edgecolor', 'none', 'facealpha', 0.2);
    end
    title(BoxPlotAxes{b},BOXPLOT_SETTINGS{b}{2});
    ylim(BoxPlotAxes{b},limits(b,:));
    grid(BoxPlotAxes{b},'on');
    ytickformat(BoxPlotAxes{b}, 'percentage'); 
end

pause(1e-3);

%% export to latex

% exporting is done via matlab2tikz  
% http://www.mathworks.com/matlabcentral/fileexchange/22022-matlab2tikz-matlab2tikz 

matlab2tikz('filename','./figures/timeseries_standalone.tex','figurehandle',ts_figure,'standalone',true,'extraAxisOptions',{'yticklabel=\pgfmathprintnumber{\tick}$\%$'});
matlab2tikz('filename','./figures/boxplot_standalone.tex','figurehandle',BoxPlotFig,'standalone',true,'extraAxisOptions',{'yticklabel=\pgfmathprintnumber{\tick}$\%$'});
matlab2tikz('filename','./figures/timeseries.tex','figurehandle',ts_figure,'extraAxisOptions',{'yticklabel=\pgfmathprintnumber{\tick}$\%$'},'parseStrings',false);
matlab2tikz('filename','./figures/boxplot.tex','figurehandle',BoxPlotFig,'extraAxisOptions',{'yticklabel=\pgfmathprintnumber{\tick}$\%$'},'parseStrings',false);

