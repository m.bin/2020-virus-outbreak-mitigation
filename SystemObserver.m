classdef SystemObserver < handle
 
    properties
        
        
        % infection
        S = [];
        E = [];
        I = []; 
        R = [];
        V = []; 
        Q = [];
        eI = [];
        R0 = 0;
        
        % contacts
        mean_number_of_contacts= 0;
        max_number_of_contacts= 0;
        median_number_of_contacts= 0;
        mean_value_of_contacts= 0;
        max_value_of_contacts= 0;
        median_value_of_contacts= 0;
          
        
    end
    
    methods
        function obj = SystemObserver()  
        end
        
        function resetCompartments(obj)
            obj.S = [];
            obj.E = [];
            obj.I = []; 
            obj.R = [];
            obj.V = []; 
            obj.Q = []; 
            obj.eI = [];
        end
        
        function addSusceptible(obj,index)
            obj.S = [obj.S;index]; 
        end
        function addExposed(obj,index) 
            obj.E = [obj.E;index]; 
        end
        function addInfected(obj,index)
            obj.I = [obj.I;index]; 
        end
        function addResolved(obj,index) 
            obj.R = [obj.R;index];
        end
        function addVaccinated(obj,index)  
            obj.V = [obj.V;index];
        end
        function addQuarantined(obj,index)
            obj.Q = [obj.Q;index];
        end
        function addEverInfected(obj,index) 
            obj.eI = [obj.eI;index];
        end
 
        
%% update   
        function updateInfectionStats(obj,Agents) 
            
            obj.resetCompartments();
            
            Ro = 0;
            n_inf = 0;
            for i=1:numel(Agents)
                if Agents{i}.isSusceptible()
                    obj.addSusceptible(i);
                elseif Agents{i}.isExposed()
                    obj.addExposed(i);
                elseif Agents{i}.isInfected()
                    obj.addInfected(i);
                elseif Agents{i}.isResolved()
                    obj.addResolved(i);
                elseif Agents{i}.isVaccinated()
                    obj.addVaccinated(i);
                end 
                 
                if Agents{i}.isQuarantined()
                    obj.addQuarantined(i);
                end
                
                if Agents{i}.hasBeenInfected()
                    obj.addEverInfected(i);
                    Ro = Ro+Agents{i}.people_infected;
                    n_inf = n_inf+1;
                end
            end 
            
            obj.R0 = Ro*pinv(n_inf);
 
        end
        

        
        function s = getNumberSusceptible(obj)
            s = numel(obj.S); 
        end
        function s = getNumberExposed(obj)
            s = numel(obj.E); 
        end
        function s = getNumberInfected(obj)
            s = numel(obj.I); 
        end
        function s = getNumberResolved(obj)
            s = numel(obj.R); 
        end
        function s = getNumberVaccinated(obj)
            s = numel(obj.V); 
        end
        function s = getNumberQuarantined(obj)
            s = numel(obj.Q); 
        end
        function s = getNumberEverInfected(obj)
            s = numel(obj.eI); 
        end
        function s = getR0(obj) 
            s = obj.R0;
        end
        
%%%%%% contacts
        
        function updateContactStats(obj,Agents)
            global N 
            nV = nan(N,1);
            vV = nan(N^2,1); 
            n = 1;
            
            for i=1:numel(Agents)
                nV(i) = numel(Agents{i}.contacts_agents); 
                
                for j=1:numel(Agents{i}.contacts_values)
                    vV(n) = Agents{i}.contacts_values(j);
                    n = n+1;
                end
                
            end
            
            nV=rmmissing(nV);
            vV=rmmissing(vV);
            
            obj.mean_number_of_contacts=mean(nV);
            obj.max_number_of_contacts=max(nV);
            obj.median_number_of_contacts=median(nV);
            
            if isempty(vV)
                obj.mean_value_of_contacts=NaN;
                obj.max_value_of_contacts=NaN;
                obj.median_value_of_contacts=NaN;
            else
                obj.mean_value_of_contacts=mean(vV);
                obj.max_value_of_contacts=max(vV);
                obj.median_value_of_contacts=median(vV);
            end
            
  
        end
        
    
       
    end
end

