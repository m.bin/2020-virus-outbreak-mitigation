classdef Distribution1D < handle
     
    
    properties
        X;
        D;
    end
    
    methods
        function obj = Distribution1D(distrib_f,distrib_span) 
            obj.processDistribution(distrib_f,distrib_span);
        end
        
        
        function processDistribution(obj,distrib_f,distrib_span)
            
            Y = distrib_f(distrib_span);
            Y = Y-min([0;Y(:)]); 
            
            obj.X = distrib_span;
            obj.D = Y./sum(Y(:)); 

        end
        
        
        function [samples] = sample(obj,n)
 
            MaxY=max(obj.D);
            samples=NaN(n,1);
            for i=1:n 
                candidates = obj.X(obj.D>=rand()*MaxY); 
                samples(i)=candidates(1+floor(rand()*length(candidates))); 
            end

        end
        
        
        function ax = plot(obj,figno)
            figure(figno);
            ax=gca;
            plot(ax,obj.X,obj.D);
        end
            
    end
end

