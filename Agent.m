classdef Agent < handle
 
    
    properties (Constant)
        %compartments
        IS_S = 0;        %0 = susceptible  
        IS_E = 1;        %1 = exposed
        IS_I = 2;        %2 = infected
        IS_R = 3;        %4 = resolved
        IS_V = 4;        %5 = vaccinated
        
               
            
    end
    
    properties
         
        % infection
        infection_status = Agent.IS_S;  
        quarantined = false;   
        was_quarantined = false;   
        quarantined_because_infected = false;
        
        time_of_infection;
        last_event_time=0;
        incubation_time; 
        resolving_time; 
        quarantine_time;        
        
         
        % network 
        Bubbles_max_sizes
        Bubbles;
            
        % contacts  
        contacts_agents = [];
        contacts_values = []; 
        contacts_times = [];
        people_infected = 0;
        has_been_infected=false;
        
        % other
        ID;  
        
        % testing
        last_test_time = NaN;                %days
        last_test_positive = false; 
        
         
    end
    
    
    
    
   
        
    
    methods
        function obj = Agent(id,incubation_time0,resolving_time0,bubs_size)
        	obj.ID = id;     
            obj.incubation_time = incubation_time0; 
            obj.resolving_time = resolving_time0;  
            obj.Bubbles_max_sizes = bubs_size;
            obj.Bubbles = cell(length(bubs_size),1);
            for i=1:length(bubs_size)
                obj.Bubbles{i}=[];
            end
        end
        
      
%%   Bubbles
        
        function addToBubble(obj,ID,k)
            obj.Bubbles{k} = unique([obj.Bubbles{k};ID]);
        end
        
        function setBubble(obj,b,k)
            obj.Bubbles{k} = b;
            obj.Bubbles_max_sizes(k)=numel(b);
        end
        
        function s = getBubble(obj,k)
            s = obj.Bubbles{k}(:);
        end
        
        function s = getBubbleSize(obj,k)
            s = length(obj.Bubbles{k});
        end
        
        % deprecated
        function s = bubbleHasSpace(obj,k)
           s = obj.getBubbleSize(k)<obj.Bubbles_max_sizes(k);
        end
        
        function s = getContainingBubble(obj,ID)
           s=-1;
           for k=1:length(obj.Bubbles)
              if arrayContains(obj.Bubbles{k},ID)
                  s=k;
                  break;
              end
           end
        end
        
%%  Infection
        
    %set
        function setExposed(obj,time)           %time in days
            obj.infection_status = Agent.IS_E;
            obj.last_event_time = time; 
        end  
        function setInfected(obj,time)
            obj.infection_status = Agent.IS_I;
            obj.last_event_time = time; 
            obj.has_been_infected=true;
            obj.time_of_infection=time;
        end 
        function setResolved(obj,time)
            obj.infection_status = Agent.IS_R;
            obj.last_event_time = time; 
        end 
        function setVaccined(obj,time)
            obj.infection_status = Agent.IS_V;
            obj.last_event_time = time; 
        end
         
	%get 
        function isit = isSusceptible(obj)
            isit =   obj.infection_status==Agent.IS_S;
        end 
        function isit = isExposed(obj)
            isit =   obj.infection_status==Agent.IS_E;
        end 
        function isit = isInfected(obj)
            isit =   obj.infection_status==Agent.IS_I;
        end 
        function isit = isResolved(obj)
            isit =   obj.infection_status==Agent.IS_R;
        end 
        function isit = isVaccinated(obj)
            isit =   obj.infection_status==Agent.IS_V;
        end
          
         
        function isit = isContagious(obj)
            isit =   obj.isInfected();
        end 
        function s = hasBeenInfected(obj)
            s = obj.has_been_infected;
        end
        function s = testsPositive(obj)
            s = obj.isInfected();
        end
        function s = eligibleForVaccine(obj)
            s = ~obj.testsPositive();
        end
        
%% Quarantine
        function activateQuarantine(obj,time,becauseI) 
            obj.quarantined = true;
            obj.was_quarantined = true;
            obj.quarantine_time = time; 
            obj.quarantined_because_infected = becauseI;
        end
        function deactivateQuarantine(obj) 
            obj.quarantined = false; 
        end
        function isit = isQuarantined(obj)
            isit = obj.quarantined;
        end
        function isit = wasQuarantined(obj)
            isit = obj.was_quarantined;
        end
        
%% update
        function update(obj,t)   %t in days
            global QUARANTINE_TIME
            
            if obj.isExposed()
                if t-obj.last_event_time > obj.incubation_time
                    obj.setInfected(t);
                end
            elseif obj.isInfected()
                if t-obj.last_event_time > obj.resolving_time
                    obj.setResolved(t); 
                end  
            end
            
            if obj.isQuarantined()
                if t-obj.quarantine_time > QUARANTINE_TIME
                   if obj.isInfected()
                       obj.activateQuarantine(t,true);
                   else
                       obj.deactivateQuarantine();
                   end
                end
            end
            
            obj.updateContactWeights();
            
        end
        
%% Contacts
        
        function newContact(obj,ID,w,t)
            global sigma sigma_inv
            if isempty(obj.contacts_agents) || ~arrayContains(obj.contacts_agents,ID) 
            	obj.contacts_agents = [obj.contacts_agents;ID]; 
                obj.contacts_values = [obj.contacts_values;sigma(w)]; 
                obj.contacts_times = [obj.contacts_times;t]; 
            else
            	k=obj.contacts_agents==ID; 
            	obj.contacts_values(k)=sigma(sigma_inv(obj.contacts_values(k))+w);
                obj.contacts_times(k) = t; 
            end
        end
                 
        function v = getContactValue(obj,target_id)
           idx = obj.contacts_agents==target_id;
           if any(idx)
               v = obj.contacts_values(idx);
           else
               v=0;
           end
        end
        
        function hasthem = hasContacts(obj)
            hasthem = ~isempty(obj.contacts_agents);
        end
        
        function nc = getNumberOfContacts(obj)
            nc = length(obj.contacts_agents);
        end       
        
        function newPersonInfected(obj) 
            obj.people_infected = obj.people_infected +1; 
        end
         
        function updateContactWeights(obj)
            global CONTACT_ERASE_THRESHOLD CONTACT_FORGETTING_FACTOR
            
            toerase=[];
            
            for k=1:length(obj.contacts_values) 
                obj.contacts_values(k)=CONTACT_FORGETTING_FACTOR*obj.contacts_values(k);
                if obj.contacts_values(k)<CONTACT_ERASE_THRESHOLD
                	toerase=[toerase;k];
                end
            end
            
            if ~isempty(toerase)
             	obj.contacts_agents(toerase)=[];
            	obj.contacts_values(toerase)=[]; 
            	obj.contacts_times(toerase)=[];
            end
        end 
     

%% Testing
          
        function testpositive = test(obj,time) 
            obj.last_test_time = time;
            testpositive =  obj.testsPositive();
            obj.last_test_positive = testpositive; 
        end
         
        function s = wasTested(obj)
            s = ~isnan(obj.last_test_time);
        end

        
    end
end

